<div style="display: flex;">
	<img src="https://oss.iteachyou.cc/logo.png" height="30" />
	<div style="margin-left: 5px; font-size: 30px; line-height: 30px; font-weight: bold;">梦想家内容管理系统</div>
</div>

----------
### 版权

* 发布本资料须遵守开放出版许可协议 1.0 或者最新版本；
* 未经版权所有者明确授权，禁止发行本文档及其被实质上修改的版本； 
* 未经版权所有者事先授权，禁止将此作品及其衍生作品以标准（纸质）书籍形式发行；
* 如果有兴趣再发行或再版本手册的全部或部分内容，不论修改过与否，或者有任何问题，请联系版权所有者 http://www.iteachyou.cc。 
* 对DreamerCMS及本手册有任何疑问或者建议，请进入官方论坛 http://bbs.iteachyou.cc 发布相关讨论。并在此感谢所有参与到DreamerCMS项目中的朋友。
* 有关DreamerCMS项目及本文档的最新资料，请及时访问官方主站 http://cms.iteachyou.cc。
* 本文档及其描述的内容受有关法律的版权保护，对本文档内容的任何形式的非法复制，泄露或散布，将导致相应的法律责任。


